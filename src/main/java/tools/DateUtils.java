package tools;

import com.google.common.collect.ImmutableSet;
import exception.UnsupportedDateException;
import model.EmployeeExperienceTemplate;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.time.format.DateTimeFormatter;
import java.util.OptionalInt;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class DateUtils {

    private static final Logger LOGGER = Logger.getLogger(DateUtils.class.getName());

    private static final Set<String> SUPPORTED_DATE_SEPARATORS = ImmutableSet.of("/", "-");
    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
    private static final int TOTAL_DATE_COMPONENTS = 3;

    public static DateTimeFormatter getDateFormatter(Set<EmployeeExperienceTemplate> experienceTemplateSet) throws UnsupportedDateException {

        String defaultSeparator = SUPPORTED_DATE_SEPARATORS.iterator().next();
        String datePattern = DEFAULT_DATE_FORMAT;

        if (CollectionUtils.isNotEmpty(experienceTemplateSet)) {
            final EmployeeExperienceTemplate sampleExperienceTemplate = experienceTemplateSet.iterator().next();
            final String sampleDate = sampleExperienceTemplate.getUnparsedDateFrom();

            final String separator = findSupportedSeparator(sampleDate);
            defaultSeparator = separator;

            final Set<String[]> dateComponentsToSet = experienceTemplateSet.stream()
                    .filter(template -> StringUtils.isNotEmpty(template.getUnparsedDateFrom()) &&
                            !template.getUnparsedDateFrom().toUpperCase().contains("NULL"))
                    .map(expTemplate -> expTemplate.getUnparsedDateFrom().split(separator)).collect(Collectors.toSet());

            datePattern = getDatePattern(dateComponentsToSet, separator, sampleDate, false);

            if (datePattern == null) {
                final Set<String[]> dateComponentsFromSet = experienceTemplateSet.stream()
                        .filter(template -> StringUtils.isNotEmpty(template.getUnparsedDateTo()) &&
                                !template.getUnparsedDateTo().toUpperCase().contains("NULL"))
                        .map(expTemplate -> expTemplate.getUnparsedDateTo().split(separator)).collect(Collectors.toSet());
                datePattern = getDatePattern(dateComponentsFromSet, separator, sampleDate, true);
            }

        }

        datePattern = checkDateFormatIntegrity(datePattern, defaultSeparator);
        return DateTimeFormatter.ofPattern(datePattern);
    }

    private static String findSupportedSeparator(String sampleDate) throws UnsupportedDateException {
        String[] dateComponents;

        for (final String dateSeparator : SUPPORTED_DATE_SEPARATORS) {
            dateComponents = sampleDate.split(dateSeparator);
            if (dateComponents.length == TOTAL_DATE_COMPONENTS) {
                return dateSeparator;
            }
        }

        throw new UnsupportedDateException(String.format("Unsupported date pattern or unrecognized separator found. " +
                "Currently supported separators are: %s", SUPPORTED_DATE_SEPARATORS));
    }

    private static String getDatePattern(Set<String[]> dateComponentsSet, String dateSeparator,
                                         String sampleDate, boolean isFallback) {

        final String[] sampleDateComponents = sampleDate.split(dateSeparator);
        final StringBuilder sb = new StringBuilder();

        boolean isFullyDecomposed = false;

        for (int index = 0; index < sampleDateComponents.length; index++) {
            if (StringUtils.isNumeric(sampleDateComponents[index])) {
                if (sampleDateComponents[index].length() == 4) {
                    sb.append("yyyy" + dateSeparator);
                } else if (sampleDateComponents[index].length() == 2) {
                    int currentIndex = index;
                    final OptionalInt max = dateComponentsSet.stream()
                            .mapToInt(expTemplate -> Integer.parseInt(expTemplate[currentIndex])).max();
                    if (max.getAsInt() > 12) {
                        sb.append("dd" + dateSeparator);
                        isFullyDecomposed = true;
                    } else {
                        sb.append("MM" + dateSeparator);
                    }
                }
            }
        }

        sb.setLength(sb.length() - 1);
        if (isFullyDecomposed || isFallback) {
            return sb.toString();
        }
        return null;
    }

    private static String checkDateFormatIntegrity(String dateFormat, String separator) {
        String result = dateFormat;

        if (dateFormat.indexOf("MM") != dateFormat.lastIndexOf("MM")) {
            result = dateFormat.replaceFirst("MM", "dd");
            LOGGER.log(Level.WARNING, String.format("The application couldn't infer the date pattern. Falling back to: %s format. " +
                    "Look on the bright side, at least you didn't get an ugly exception.", result));

        }
        return result;
    }
}
