package exception;

public class UnsupportedDateException extends Exception {

    private static final long serialVersionUID = -8823626609939146153L;

    public UnsupportedDateException() {
    }

    public UnsupportedDateException(String message) {
        super(message);
    }

    public UnsupportedDateException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnsupportedDateException(Throwable cause) {
        super(cause);
    }

    public UnsupportedDateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
