package calculator;

import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import model.EmployeeCommonExperienceRow;
import model.EmployeeExperienceRecord;

import java.time.LocalDate;
import java.util.*;

import static java.time.temporal.ChronoUnit.DAYS;

public class CommonExperienceCalculator {

    private static final String KEY_SEPARATOR = ",";

    public static Set<EmployeeCommonExperienceRow> calculateCommonExperienceRows(Set<EmployeeExperienceRecord> experienceRecordSet) {

        final Set<EmployeeCommonExperienceRow> result = new HashSet<>();
        final Map<Long, Set<EmployeeExperienceRecord>> projectEmployeeRecordsMap = getProjectEmployeeRecordsMap(experienceRecordSet);

        for (final Set<EmployeeExperienceRecord> employeeExperienceRecordSet : projectEmployeeRecordsMap.values()) {
            if (employeeExperienceRecordSet.size() > 1) {
                final Iterator<EmployeeExperienceRecord> recordIterator = employeeExperienceRecordSet.iterator();
                final EmployeeExperienceRecord currentRecord = recordIterator.next();
                addCommonExperienceRows(result, currentRecord, recordIterator);
            }
        }
        return result;
    }

    private static void addCommonExperienceRows(Set<EmployeeCommonExperienceRow> commonExperienceRowSet, EmployeeExperienceRecord currentRecord,
                                                Iterator<EmployeeExperienceRecord> recordIterator) {
        if (recordIterator.hasNext()) {
            final EmployeeExperienceRecord nextRecord = recordIterator.next();
            if (currentRecord.getDateTo().isAfter(nextRecord.getDateFrom()) && currentRecord.getDateFrom().isBefore(nextRecord.getDateTo())) {
                commonExperienceRowSet.add(getCommonExperienceRow(currentRecord, nextRecord));
            }
            addCommonExperienceRows(commonExperienceRowSet, nextRecord, recordIterator);
        }

    }

    private static EmployeeCommonExperienceRow getCommonExperienceRow(EmployeeExperienceRecord firstRecord, EmployeeExperienceRecord secondRecord) {

        final LocalDate rangeStart = Ordering.natural().max(firstRecord.getDateFrom(), firstRecord.getDateFrom());
        final LocalDate rangeEnd = Ordering.natural().min(firstRecord.getDateTo(), secondRecord.getDateTo());

        long daysBetween = DAYS.between(rangeStart, rangeEnd);

        return new EmployeeCommonExperienceRow(firstRecord.getEmpId(), secondRecord.getEmpId(), firstRecord.getProjectId(), daysBetween);

    }

    private static Map<Long, Set<EmployeeExperienceRecord>> getProjectEmployeeRecordsMap(Set<EmployeeExperienceRecord> experienceRecordSet) {

        final Map<Long, Set<EmployeeExperienceRecord>> projectEmployeeRecordsMap = new HashMap<>();

        for (final EmployeeExperienceRecord record : experienceRecordSet) {
            if (!projectEmployeeRecordsMap.containsKey(record.getProjectId())) {
                projectEmployeeRecordsMap.put(record.getProjectId(), Sets.newHashSet(record));
            } else {
                projectEmployeeRecordsMap.get(record.getProjectId()).add(record);
            }
        }

        return projectEmployeeRecordsMap;
    }

    public static EmployeeCommonExperienceRow getMaxCommonExperience(Set<EmployeeCommonExperienceRow> commonExperienceRowSet) {
        final Map<String, Long> employeeDaysInCommonMap = new HashMap<>();

        for (final EmployeeCommonExperienceRow employeeCommonExperienceRow : commonExperienceRowSet) {
            long empId1 = employeeCommonExperienceRow.getEmployeeId1();
            long empId2 = employeeCommonExperienceRow.getEmployeeId2();
            String key;
            if (empId1 < empId2) {
                key = empId1 + KEY_SEPARATOR + empId2;
            } else {
                key = empId2 + KEY_SEPARATOR + empId1;
            }

            if (!employeeDaysInCommonMap.containsKey(key)) {
                employeeDaysInCommonMap.put(key, employeeCommonExperienceRow.getDaysWorked());
            } else {
                long currentValue = employeeDaysInCommonMap.get(key);
                employeeDaysInCommonMap.put(key, employeeCommonExperienceRow.getDaysWorked() + currentValue);
            }
        }

        final Map.Entry<String, Long> maxEntry = employeeDaysInCommonMap.entrySet().stream().max(Map.Entry.comparingByValue()).get();
        final String[] ids = maxEntry.getKey().split(KEY_SEPARATOR);

        return new EmployeeCommonExperienceRow(Long.parseLong(ids[0]), Long.parseLong(ids[1]), 0L, maxEntry.getValue());

    }

}
