import calculator.CommonExperienceCalculator;
import exception.UnsupportedDateException;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.geometry.Insets;
import model.EmployeeCommonExperienceRow;
import model.EmployeeExperienceRecord;
import model.EmployeeExperienceTemplate;
import tools.DateUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.format.DateTimeFormatter;
import java.util.Set;
import java.util.stream.Collectors;

public class CommonExperienceApp extends Application {
    private final TableView table = new TableView();
    private final FileChooser fileChooser = new FileChooser();
    private final Label label = new Label();
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        final Scene scene = new Scene(new Group());
        stage.setTitle("Employees Common Experience Calculator");
        stage.setWidth(830);
        stage.setHeight(610);



        final Button openButton = new Button("Select a file...");

        openButton.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(final ActionEvent e) {
                        File file = fileChooser.showOpenDialog(stage);
                        if (file != null) {
                            try {
                                openFile(file);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    }
                });
        final GridPane inputGridPane = new GridPane();

        GridPane.setConstraints(openButton, 0, 0);
        inputGridPane.setHgap(6);
        inputGridPane.setVgap(6);
        inputGridPane.getChildren().addAll(openButton);

        table.setEditable(true);
        table.setMinWidth(800);
        table.setMinHeight(510);

        final TableColumn employeeId1Col = new TableColumn("Employee ID #1");
        employeeId1Col.setPrefWidth(195);
        employeeId1Col.setResizable(false);
        employeeId1Col.setCellValueFactory(new PropertyValueFactory<EmployeeCommonExperienceRow, String>("employeeId1"));
        final TableColumn employeeId2Col = new TableColumn("Employee ID #2");
        employeeId2Col.setPrefWidth(195);
        employeeId2Col.setResizable(false);
        employeeId2Col.setCellValueFactory(new PropertyValueFactory<EmployeeCommonExperienceRow, String>("employeeId2"));
        final TableColumn projectIdCol = new TableColumn("Project ID");
        projectIdCol.setPrefWidth(195);
        projectIdCol.setResizable(false);
        projectIdCol.setCellValueFactory(new PropertyValueFactory<EmployeeCommonExperienceRow, String>("projectId"));
        final TableColumn daysWorkedCol = new TableColumn("Days worked");
        daysWorkedCol.setPrefWidth(195);
        daysWorkedCol.setResizable(false);
        daysWorkedCol.setCellValueFactory(new PropertyValueFactory<EmployeeCommonExperienceRow, String>("daysWorked"));

        table.getColumns().addAll(employeeId1Col, employeeId2Col, projectIdCol, daysWorkedCol);

        final VBox vbox = new VBox();
        vbox.setSpacing(5);
        vbox.setPadding(new Insets(10, 0, 0, 10));
        vbox.getChildren().addAll(table, inputGridPane, label);

        ((Group) scene.getRoot()).getChildren().addAll(vbox);

        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    private void openFile(File file) throws UnsupportedDateException, IOException {

        final Path path = Paths.get(file.getAbsolutePath());

        final Set<EmployeeExperienceTemplate> employeeExperienceTemplateSet = Files.lines(path, StandardCharsets.UTF_8)
                .map(line -> EmployeeExperienceTemplate.getTemplate(line)).filter(template -> template != null).collect(Collectors.toSet());

        final DateTimeFormatter dateTimeFormatter = DateUtils.getDateFormatter(employeeExperienceTemplateSet);

        final Set<EmployeeExperienceRecord> employeeExperienceRecordSet = employeeExperienceTemplateSet.stream()
                .map(template -> EmployeeExperienceRecord.getRecordFromTemplate(template, dateTimeFormatter))
                .collect(Collectors.toSet());

        final Set<EmployeeCommonExperienceRow> commonExperienceRows = CommonExperienceCalculator.calculateCommonExperienceRows(employeeExperienceRecordSet);

        final EmployeeCommonExperienceRow maxCommonExp = CommonExperienceCalculator.getMaxCommonExperience(commonExperienceRows);

        final ObservableList<EmployeeCommonExperienceRow> rows = FXCollections.observableArrayList();
        rows.addAll(commonExperienceRows);

        table.setItems(rows);
        label.setText(String.format("Employees Ids with more common experience are id %s and %s with %s days.",
                maxCommonExp.getEmployeeId1(), maxCommonExp.getEmployeeId2(), maxCommonExp.getDaysWorked()));
    }
}
