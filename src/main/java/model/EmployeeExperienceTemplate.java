package model;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

public final class EmployeeExperienceTemplate extends AbstractEmployeeExperience {

    String unparsedDateFrom;

    String unparsedDateTo;

    private EmployeeExperienceTemplate(long empId, long projectId, String unparsedDateFrom, String unparsedDateTo) {
        super(empId, projectId);
        this.unparsedDateFrom = unparsedDateFrom;
        this.unparsedDateTo = unparsedDateTo;
    }

    public String getUnparsedDateFrom() {
        return unparsedDateFrom;
    }

    public void setUnparsedDateFrom(String unparsedDateFrom) {
        this.unparsedDateFrom = unparsedDateFrom;
    }

    public String getUnparsedDateTo() {
        return unparsedDateTo;
    }

    public void setUnparsedDateTo(String unparsedDateTo) {
        this.unparsedDateTo = unparsedDateTo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        EmployeeExperienceTemplate that = (EmployeeExperienceTemplate) o;
        return Objects.equals(unparsedDateFrom, that.unparsedDateFrom) &&
                Objects.equals(unparsedDateTo, that.unparsedDateTo);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), unparsedDateFrom, unparsedDateTo);
    }

    @Override
    public String toString() {
        return "EmployeeExperienceTemplate{" +
                "empId=" + empId + '\'' +
                ", projectId=" + projectId + '\'' +
                ", unparsedDateFrom='" + unparsedDateFrom + '\'' +
                ", unparsedDateTo='" + unparsedDateTo + '\'' +
                '}';
    }

    public static EmployeeExperienceTemplate getTemplate(String line) {
        final String[] fields = line.trim().replaceAll("\\s", "").split(",");
        if(ArrayUtils.isNotEmpty(fields) && fields.length == 4) {
            final long employeeId = Long.parseLong(fields[0]);
            final long projectId = Long.parseLong(fields[1]);
            final String unparsedFrom = fields[2];
            final String unparsedTo = StringUtils.isEmpty(fields[3]) || fields[3].toUpperCase().contains("NULL") ? null : fields[3];

            return new EmployeeExperienceTemplate(employeeId, projectId, unparsedFrom, unparsedTo);
        }
        return null;
    }
}
