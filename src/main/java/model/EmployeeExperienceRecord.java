package model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public final class EmployeeExperienceRecord extends AbstractEmployeeExperience {

    LocalDate dateFrom;

    LocalDate dateTo;

    private EmployeeExperienceRecord(long empId, long projectId, LocalDate dateFrom, LocalDate dateTo) {
        super(empId, projectId);
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public void setDateTo(LocalDate dateTo) {
        this.dateTo = dateTo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        EmployeeExperienceRecord that = (EmployeeExperienceRecord) o;
        return Objects.equals(dateFrom, that.dateFrom) &&
                Objects.equals(dateTo, that.dateTo);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), dateFrom, dateTo);
    }

    @Override
    public String toString() {
        return "EmployeeExperienceRecord{" +
                "empId=" + empId +
                ", projectId=" + projectId +
                ", dateFrom=" + dateFrom +
                ", dateTo=" + dateTo +
                '}';
    }

    public static EmployeeExperienceRecord getRecordFromTemplate(EmployeeExperienceTemplate template, DateTimeFormatter dateTimeFormatter) {

        final LocalDate dateFrom = LocalDate.parse(template.getUnparsedDateFrom(), dateTimeFormatter);
        final LocalDate dateTo = template.getUnparsedDateTo() == null ?
                LocalDate.now() : LocalDate.parse(template.getUnparsedDateTo(), dateTimeFormatter);

        return new EmployeeExperienceRecord(template.getEmpId(), template.getProjectId(), dateFrom, dateTo);

    }
}
