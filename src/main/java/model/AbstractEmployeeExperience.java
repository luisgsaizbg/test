package model;

import java.util.Objects;
/*
 * Abstract class to avoid accidental instantiation
 */
public abstract class AbstractEmployeeExperience {

    long empId;

    long projectId;

    protected AbstractEmployeeExperience(long empId, long projectId) {
        this.empId = empId;
        this.projectId = projectId;
    }

    public long getEmpId() {
        return empId;
    }

    public void setEmpId(long empId) {
        this.empId = empId;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractEmployeeExperience that = (AbstractEmployeeExperience) o;
        return empId == that.empId &&
                projectId == that.projectId;
    }

    @Override
    public int hashCode() {

        return Objects.hash(empId, projectId);
    }

    @Override
    public String toString() {
        return "AbstractEmployeeExperience{" +
                "empId=" + empId +
                ", projectId=" + projectId +
                '}';
    }
}
