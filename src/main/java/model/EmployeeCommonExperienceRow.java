package model;

import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

import java.util.Objects;

public class EmployeeCommonExperienceRow {

    private final SimpleLongProperty employeeId1;

    private final SimpleLongProperty employeeId2;

    private final SimpleLongProperty projectId;

    private final SimpleLongProperty daysWorked;

    public EmployeeCommonExperienceRow(long employeeId1, long employeeId2, long projectId, long daysWorked) {
        this.employeeId1 = new SimpleLongProperty(employeeId1);
        this.employeeId2 =  new SimpleLongProperty(employeeId2);
        this.projectId =  new SimpleLongProperty(projectId);
        this.daysWorked =  new SimpleLongProperty(daysWorked);
    }

    public long getEmployeeId1() {
        return employeeId1.get();
    }

    public void setEmployeeId1(long employeeId1) {
        this.employeeId1.set(employeeId1);
    }

    public long getEmployeeId2() {
        return employeeId2.get();
    }

    public void setEmployeeId2(long employeeId2) {
        this.employeeId2.set(employeeId2);
    }

    public long getProjectId() {
        return projectId.get();
    }

    public void setProjectId(long projectId) {
        this.projectId.set(projectId);
    }

    public long getDaysWorked() {
        return daysWorked.get();
    }

    public void setDaysWorked(long daysWorked) {
        this.daysWorked.set(daysWorked);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmployeeCommonExperienceRow that = (EmployeeCommonExperienceRow) o;
        return employeeId1.get() == that.employeeId1.get() &&
                employeeId2.get() == that.employeeId2.get() &&
                projectId.get() == that.projectId.get() &&
                daysWorked.get() == that.daysWorked.get();
    }

    @Override
    public int hashCode() {

        return Objects.hash(employeeId1, employeeId2, projectId, daysWorked);
    }

    @Override
    public String toString() {
        return "EmployeeCommonExperienceRow{" +
                "employeeId1=" + employeeId1 +
                ", employeeId2=" + employeeId2 +
                ", projectId=" + projectId +
                ", daysWorked=" + daysWorked +
                '}';
    }
}
