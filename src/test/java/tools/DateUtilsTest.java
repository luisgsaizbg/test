package tools;

import com.google.common.collect.ImmutableSet;
import exception.UnsupportedDateException;
import model.EmployeeExperienceTemplate;
import org.junit.Before;
import org.junit.Test;

import java.time.format.DateTimeFormatter;

import static org.junit.Assert.*;

public class DateUtilsTest {

    EmployeeExperienceTemplate emp1;
    EmployeeExperienceTemplate emp2;
    EmployeeExperienceTemplate emp3;
    EmployeeExperienceTemplate emp4;

    @Before
    public void setUp() throws Exception {
        emp1 = EmployeeExperienceTemplate.getTemplate("43, 12, 2013-11-01, 2014-01-05");
        emp2 = EmployeeExperienceTemplate.getTemplate("42, 12, 2013-11-06, 2014-02-30");
        emp3 = EmployeeExperienceTemplate.getTemplate("41, 12, 2012-10-01, 2015-01-11");
        emp4 = EmployeeExperienceTemplate.getTemplate("45, 12, 2012@10@01, 2015@01@12");
    }

    @Test
    public void getDateFormatter() throws UnsupportedDateException {

        DateTimeFormatter result = DateUtils.getDateFormatter(ImmutableSet.of(emp1, emp2));

        assertNotNull("The result should not be null", result);
    }

    @Test
    public void getDateFormatterFallback() throws UnsupportedDateException {

        DateTimeFormatter result = DateUtils.getDateFormatter(ImmutableSet.of(emp1, emp3));

        assertNotNull("The result should not be null", result);
    }

    @Test(expected = UnsupportedDateException.class)
    public void getDateFormatterException() throws UnsupportedDateException {

        DateTimeFormatter result = DateUtils.getDateFormatter(ImmutableSet.of(emp4));
    }
}