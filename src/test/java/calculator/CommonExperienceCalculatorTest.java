package calculator;

import com.google.common.collect.ImmutableSet;
import model.EmployeeCommonExperienceRow;
import model.EmployeeExperienceRecord;
import model.EmployeeExperienceTemplate;
import org.junit.Before;
import org.junit.Test;

import java.time.format.DateTimeFormatter;
import java.util.Set;

import static org.junit.Assert.*;

public class CommonExperienceCalculatorTest {

    EmployeeExperienceRecord record1;
    EmployeeExperienceRecord record2;

    EmployeeCommonExperienceRow row1;
    EmployeeCommonExperienceRow row2;
    EmployeeCommonExperienceRow row3;
    EmployeeCommonExperienceRow row4;

    EmployeeCommonExperienceRow expectedRow;
    EmployeeCommonExperienceRow expectedMax;

    @Before
    public void setUp() throws Exception {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        EmployeeExperienceTemplate emp1 = EmployeeExperienceTemplate.getTemplate("43, 12, 2013-11-01, 2014-01-05");
        EmployeeExperienceTemplate emp2 = EmployeeExperienceTemplate.getTemplate("42, 12, 2013-11-06, 2014-02-30");
        EmployeeExperienceTemplate emp3 = EmployeeExperienceTemplate.getTemplate("4, 10, 2011-11-01, 2015-01-05");
        EmployeeExperienceTemplate emp4 = EmployeeExperienceTemplate.getTemplate("3, 10, 2010-11-06, 2016-02-30");

        record1 = EmployeeExperienceRecord.getRecordFromTemplate(emp1, dateTimeFormatter);
        record2 = EmployeeExperienceRecord.getRecordFromTemplate(emp2, dateTimeFormatter);

        row1 = new EmployeeCommonExperienceRow(1,2, 1, 50);
        row2 =  new EmployeeCommonExperienceRow(3,4, 11, 100);
        row3 = new EmployeeCommonExperienceRow(1,2, 10, 51);
        row4=  new EmployeeCommonExperienceRow(5,8, 4, 99);

        expectedMax = new EmployeeCommonExperienceRow(1, 2, 0, 101);
        expectedRow = new EmployeeCommonExperienceRow(43, 42, 12L, 65);
    }

    @Test
    public void calculateCommonExperienceRows() {

        Set<EmployeeCommonExperienceRow> result = CommonExperienceCalculator
                .calculateCommonExperienceRows(ImmutableSet.of(record1, record2));

        assertNotNull("Result should not be null.", result);
        assertEquals("The result is not the expected.", result.iterator().next(), expectedRow);

    }

    @Test
    public void getMaxCommonExperience() {

        EmployeeCommonExperienceRow result = CommonExperienceCalculator
                .getMaxCommonExperience(ImmutableSet.of(row1, row2, row3, row4));

        assertNotNull("Result should not be null.", result);
        assertEquals("The result is not the expected.", result, expectedMax);
    }
}